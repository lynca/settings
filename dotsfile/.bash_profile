alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
if [ -f ~/.bashrc ] ; then
. ~/.bashrc
fi
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/opt/X11/bin

#power-shell
function _update_ps1() {
    export PS1=""
}
export PROMPT_COMMAND="_update_ps1; "
#power-shell

