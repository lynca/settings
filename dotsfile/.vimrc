"~~~~~~include~~~~~
set mouse=a
set ttymouse=xterm
"~~~~~~エンコード~~~~~~
set encoding=utf-8 "文字エンコード
set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
set fileformats=mac,unix,dos
scriptencoding utf-8 
"~~~~~~表示~~~~~~
set number "行番号を表示
set title "編集のファイル名表示
set wildmenu "コマンドモードの補完
set history=5000 "保存するコマンド履歴の数
let loaded_matchparen = 1 " カッコのインデント省略
set nobackup
"~~~~~~インデント~~~~~~
set smartindent "オートインデント
set expandtab
set tabstop=2 "画面上でタブ文字が占める幅
set shiftwidth=2 "自動インデントでずれる幅

"~~~~~~検索設定~~~~~~
set ignorecase
set smartcase
set wrapscan
set hlsearch

"~~~~~~keybind~~~~~
inoremap <silent> jj <ESC>
inoremap <silent> っｊ <ESC>
inoremap <silent> っj <ESC>
set backspace=indent,eol,start

"~~~~~~dein~~~~~
if &compatible
  set nocompatible
endif
" dein.vimインストール時に指定したディレクトリをセット
let s:dein_dir = expand('~/.vim/dein')

" dein.vimの実体があるディレクトリをセット
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

" dein.vimが存在していない場合はgithubからclone
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

call dein#begin(expand('~/.vim/dein'))
call dein#add('Shougo/dein.vim')
if has('lua')
  call dein#add('Shougo/neocomplete.vim')
  let is_neocomplete = 1
  else
  let is_neocomplete = 0
endif
call dein#add('itchyny/lightline.vim')
call dein#add('scrooloose/nerdtree')
call dein#add('nathanaelkane/vim-indent-guides')
call dein#add('tomasr/molokai')

call dein#end()
call dein#save_state()

if dein#check_install()
  call dein#install()
endif
"~~~~~colorscheme~~~~~
syntax on "色分け
colorscheme molokai "カラースキム
set t_Co=256

"~~~~~~vim-indent-guides~~~~~
" Vim 起動時 vim-indent-guides を自動起動
let g:indent_guides_enable_on_vim_startup=1
" ガイドをスタートするインデントの量
let g:indent_guides_start_level=2
" 自動カラー無効
let g:indent_guides_auto_colors=0
" 奇数番目のインデントの色
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#444433 ctermbg=black
" 偶数番目のインデントの色
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#333344 ctermbg=darkgray
" ガイドの幅
let g:indent_guides_guide_size = 1
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']

"~~~~~NERDTree~~~~~
if !argc()
  autocmd vimenter * NERDTree|normal gg3j
endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeDirArrows = 0
let g:NERDTreeDirArrowExpandable  = '+'
let g:NERDTreeDirArrowCollapsible = '-'
"~~~~~lightline~~~~~
set laststatus=2

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
      \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }
"~~~~~neocomplete~~~~~
if is_neocomplete == 1  
    " Vim起動時にneocompleteを有効にする
    let g:neocomplete#enable_at_startup = 1
    " smartcase有効化. 大文字が入力されるまで大文字小文字の区別を無視する
    let g:neocomplete#enable_smart_case = 1
    " 3文字以上の単語に対して補完を有効にする
    let g:neocomplete#min_keyword_length = 3
    " 区切り文字まで補完する
    let g:neocomplete#enable_auto_delimiter = 1
    " 1文字目の入力から補完のポップアップを表示
    let g:neocomplete#auto_completion_start_length = 1
    " バックスペースで補完のポップアップを閉じる
    inoremap <expr><BS> neocomplete#smart_close_popup()."<C-h>"
endif
